<div>
    <h1>Sitio Barbie-Oppenheimer</h1>
</div>

# Acerca de mí
¡Hola!

Soy Andree, estoy creando mi portafolio en [andreemalerva.com](http://www.andreemalerva.com/), actualmente soy Desarrollador Front End, y me agrada seguir aprendiendo.

Trabajemos juntos, te dejo por acá mi contacto.

```
📩 andreemalerva@gmail.com
📲 2283530727
```

# Acerca del proyecto

Este sitio esta hecho con la ayuda de tecnologías como lo son HTML, CSS, Javascript, acompañado de frameworks como Bootstrap, y jquery.

Puede ser visualizado en: [Demo for Andree Malerva | Barbie-Oppenheimer](https://sitio-barbie-open-haimer-andreemalerva-c0f546288941629a64a9c86a.gitlab.io/)

# Politícas de privacidad

```
Las imagenes, archivos css, html y adicionales son propiedad de © 2023 LYAM ANDREE CRUZ MALERVA.
```