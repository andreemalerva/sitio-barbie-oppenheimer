
var lastTime = 0;
var prefixes = 'webkit moz ms o'.split(' ');
// get unprefixed rAF and cAF, if present
var requestAnimationFrame = window.requestAnimationFrame;
var cancelAnimationFrame = window.cancelAnimationFrame;
// loop through vendor prefixes and get prefixed rAF and cAF
var prefix;
for( var i = 0; i < prefixes.length; i++ ) {
    if ( requestAnimationFrame && cancelAnimationFrame ) {
        break;
    }
    prefix = prefixes[i];
    requestAnimationFrame = requestAnimationFrame || window[ prefix + 'RequestAnimationFrame' ];
    cancelAnimationFrame  = cancelAnimationFrame  || window[ prefix + 'CancelAnimationFrame' ] ||
                              window[ prefix + 'CancelRequestAnimationFrame' ];
}
// fallback to setTimeout and clearTimeout if either request/cancel is not supported
if ( !requestAnimationFrame || !cancelAnimationFrame ) {
    requestAnimationFrame = function( callback, element ) {
        var currTime = new Date().getTime();
        var timeToCall = Math.max( 0, 16 - ( currTime - lastTime ) );
        var id = window.setTimeout( function() {
            callback( currTime + timeToCall );
        }, timeToCall );
        lastTime = currTime + timeToCall;
        return id;
    };
    cancelAnimationFrame = function( id ) {
        window.clearTimeout( id );
    };
}
// put in global namespace
window.requestAnimationFrame = requestAnimationFrame;
window.cancelAnimationFrame = cancelAnimationFrame;
//END OF requestAnimationFrame polyfill

var stage = document.getElementById('stage'), stageCtx = stage.getContext('2d');//Save canvas stage to variable
//stage.width = window.innerWidth - 20, stage.height = window.innerHeight - 20;
stage.width = window.innerWidth - 20, stage.height = window.innerHeight - 20;
//Set the word to be drawn, and space between each particle
var bgWord = 'Oppenheimer',
    space = 6,
    speed = 20,
    size = 4,
    finishBuffer = 15,
    moveX,
    moveY,
    animatePart,
    partPos = {};

var init = function() {
//Fill the word on the canvas using text
stageCtx.fillStyle = "rgba(255, 255, 255, 0.8)";
stageCtx.font = "200px impact";
stageCtx.textAlign = "center";
stageCtx.fillText(bgWord, (stage.width / 2), (stage.height / 2) + 160);

//Random number function
function randomInt(min, max) {
  return Math.random() * (max - min) + min;
}

var manageParticles = {
  coords: function() {
    //Get the image data for the entire canvas
    var imgData = stageCtx.getImageData(0, 0, stage.width, stage.height),
    pixPos,
    h,
    w,
    partTotal = 0;
    stage.width = stage.width;
    //Loop through every pixel on the canvas, leaving the space between
    for (h = 0; h < stage.height; h += space) {
      for (w = 0; w < stage.width; w += space) {
        pixPos = imgData.data[((stage.width * h) + w) * 4];
        if (pixPos == 255) {
          moveX = true;
          moveY = true;
          if (Math.round(randomInt(0, 1)) == 0) {
            moveX = false;
          }
          if (Math.round(randomInt(0, 1)) == 0) {
            moveY = false;
          }
          
          var partXSpeed = randomInt(0, speed), partYSpeed = randomInt(0, speed);
          
          partPos[partTotal] = {'x': w, 'y': h, 'startX': w, 'startY': h, 'xSpeed': partXSpeed, 'ySpeed': partYSpeed, 'moveX': moveX, 'moveY': moveY, 'end': false, 'reset': false};
          partTotal++;
        }
      }
    }
    manageParticles.render(partPos, partTotal);
  },
  render: function(partPos, partTotal) {
    
    for (var i = 0; i < partTotal; i++) {
      stageCtx.fillStyle = "rgba(255, 255, 255, 0.4)";
      stageCtx.beginPath();
      stageCtx.arc(partPos[i].x, partPos[i].y, size, 0, Math.PI*2, true);
      stageCtx.closePath();
      stageCtx.fill();
    }
    manageParticles.rearrange(partPos, partTotal);
  },
  rearrange: function(partPos, partTotal) {
    stage.addEventListener('click', audio, false, );
    function move() {
      stage.width = stage.width;
      var resetTotal = 0;
      for (var i = 0; i < partTotal; i++) {
        if (partPos[i].moveX == true) {
          if (partPos[i].reset == false) {
            partPos[i].x += partPos[i].xSpeed;
          }
        } else {
          if (partPos[i].reset == false) {
            partPos[i].x -= partPos[i].xSpeed;
          }
        }
        
        if (partPos[i].moveY == true) {
          if (partPos[i].reset == false) {
            partPos[i].y += partPos[i].ySpeed;
          }
        } else {
          if (partPos[i].reset == false) {
            partPos[i].y -= partPos[i].ySpeed;
          }
        }
        
        //Check if X and Y is bigger or smaller than startX and Y, and change direction depending
        //If X and Y is equal then set particle as reset
        //If rest total == particle total then end animation
        
        if (partPos[i].x >= stage.width) {
          //partPos[i].moveX = false;
          if (partPos[i].x >= partPos[i].startX) {
            partPos[i].moveX = false;
            partPos[i].end = true;
          } else {
            partPos[i].moveX = true;
            partPos[i].end = true;
          }
        }
        if (partPos[i].x <= 0) {
          //partPos[i].moveX = true;
          if (partPos[i].x < partPos[i].startX) {
            partPos[i].moveX = true;
            partPos[i].end = true;
          } else {
            partPos[i].moveX = false;
            partPos[i].end = true;
          }
        }
        if (partPos[i].y >= stage.height) {
          //partPos[i].moveY = false;
          if (partPos[i].y >= partPos[i].startY) {
            partPos[i].moveY = false;
            partPos[i].end = true;
          } else {
            partPos[i].moveY = true;
            partPos[i].end = true;
          }
        }
        if (partPos[i].y <= 0) {
          //partPos[i].moveY = true;
          if (partPos[i].y < partPos[i].startY) {
            partPos[i].moveY = true;
            partPos[i].end = true;
          } else {
            partPos[i].moveY = false;
            partPos[i].end = true;
          }
        }
        
        if (partPos[i].reset == false && partPos[i].end == true) {
          if (partPos[i].x < partPos[i].startX + finishBuffer && partPos[i].x > partPos[i].startX - finishBuffer && partPos[i].y < partPos[i].startY + finishBuffer && partPos[i].y > partPos[i].startY - finishBuffer) {
            partPos[i].reset = true;
            resetTotal++;
          }
        }
        if (partPos[i].reset == true) {
          stageCtx.fillStyle = "rgba(255, 255, 255, 0.4)";
        } else {
          stageCtx.fillStyle = "rgba(255, 255, 255, 0.2)";
        }
        stageCtx.beginPath();
        stageCtx.closePath();
        stageCtx.arc(partPos[i].x, partPos[i].y, size, 0, Math.PI*2, true);
        stageCtx.fill();
        
        if (resetTotal == partTotal) {
          window.cancelAnimationFrame(move);
        }
      }
      animatePart = requestAnimationFrame(move);
    }

    function audio() {
        var audio = document.getElementById("audio");
        audio.play();
        move();
    }
  }
}
  manageParticles.coords();
}
init();
